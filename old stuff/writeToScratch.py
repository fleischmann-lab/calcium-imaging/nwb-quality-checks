import statistics
import math
import matplotlib.pyplot as plt

import numpy as np
from pynwb import NWBFile
from pynwb import NWBHDF5IO

# from std import nwb_metric
import os
import warnings
from matplotlib import interactive
import scipy
from scipy import stats, signal
import pandas as pd
import plotly.graph_objects as go
from scipy.signal import find_peaks
from datetime import datetime


def file_exists(nwbfile, name):
    exists = True
    try:
        nwbfile.get_scratch(name)
    except:
        exists = False
    return exists


# def name_incrementer(name, first):
#     if first:
#         return name[:-4]+"(1).nwb"
#     else:
#         return name[:-6]+str(int(name[-6])+1)+".nwb"


warnings.filterwarnings("ignore")

# filepath = "flask/nwb_files/test.nwb"

from TimeSeriesData import *

filepath = "../mouse/Mouse#8.nwb"

io = NWBHDF5IO(filepath, mode="r+")
nwbfile = io.read()

first = False
try:
    old = nwbfile.get_scratch("0")
except:
    print("nothing here")
    first = True

if first:
    filler_data = [
        "{flow [series of tests, results, 06/09/2020 09:28:49], wheel: [series of tests, results, date]}",
        "{flow [series of tests2, results2, 06/09/2020 09:28:50], wheel2: [series of tests2, results2, date2]}",
    ]

    nwbfile.add_scratch(
        filler_data,
        name="0",
        notes="a log of results from running quality checks on the data",
    )
    io.write(nwbfile)
    io.close()

else:
    print("i recognize that this is not the first run of quality checks")
    new = "{flow [series of tests3, results3, 06/09/2020 09:28:50], wheel3: [series of tes3, results3, date3]}"
    i = "0"
    while file_exists(nwbfile, i):
        i = str(int(i) + 1)

    curr = np.append(
        np.array(nwbfile.scratch[str(int(i) - 1)].data), np.array([new])
    ).tolist()

    nwbfile.add_scratch(
        curr, name=i, notes="a log of results from running quality checks on the data"
    )
    io.write(nwbfile)
    io.close()

    # nwbfile.scratch['quality_check'].data=old+[new]

#  nwbfile.add_scratch(old + [new], name='quality_check', notes='a log of all results from running quality checks on the data')
#     io.write(nwbfile)


# io.close()

io = NWBHDF5IO(filepath, mode="r")


nwbfile = io.read()

print(nwbfile.get_scratch("0"))

print("currently the data in the data field is: ", np.array(nwbfile.scratch["3"].data))


"""
idea: try except loop to figure out the name that doesn't exist yet
"""


# nwb_scratch = nwb_proc_in.copy()

# quality_checks_history = []

# nwb_scratch.add_scratch(['testing', 'again'], name='quality_check', notes='a log of all results from running quality checks on the data')

# with NWBHDF5IO('../mouse/mouse#7_qc.nwb', 'w', manager=nwbfile.manager) as io:
#     io.write(nwb_scratch)


# scratch_io = NWBHDF5IO('../mouse/mouse#7_qc.nwb', 'r')
# nwb_scratch_in = scratch_io.read()


# fft_in = nwb_scratch_in.get_scratch('quality_check')

# error = False
# try:
#     nwb_scratch_in.get_scratch('tester')
# except:
#     print("nothing here")
#     error = True


# print("error is ", error)


# print(fft_in)


def file_exists(nwbfile, name):
    exists = True
    try:
        nwbfile.get_scratch(name)
    except:
        exists = False
