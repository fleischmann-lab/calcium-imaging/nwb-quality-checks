from pynwb import NWBFile
from pynwb import NWBHDF5IO
from std import nwb_metric
import os
import numpy as np
import matplotlib.pyplot as plt
import warnings
from matplotlib import interactive

# ignore annoying warnings
warnings.filterwarnings("ignore")

filepath = "flask/nwb_files/test.nwb"


try:
    io = NWBHDF5IO(filepath, "r")
except:
    print("not a valid file path")

nwbfile = io.read()

data_container = vars(nwbfile)["_AbstractContainer__children"]


for i in range(len(data_container)):
    objdata = data_container[i]
    if objdata.neurodata_type == "TimeSeries":
        print("\n")

        checker = nwb_metric(objdata)
        data = checker.get_filtered_data()
        name = checker.get_data_name()

        # if specified and (str(name) not in desired_sets):
        #     continue

        print("data name: ", name)
        print("sample 10 data points: \n    ", data[:10])
        print("the data type of each data point is: ", type(data[0]))
        print("data consists of ", len(objdata.data), " data points")
        print("Standard Deviation is: \n    ", checker.check_spread())
        print("Saturation is: \n    ", checker.check_saturation())
        print("Prevalence of jumps: \n    ", checker.prob_jumps())
        print("\n")

        plt.plot(data)
        plt.xlabel(str(objdata.timestamps_unit))
        plt.ylabel(str(objdata.unit))
        plt.title(str(name))
        plt.show()
