import statistics
import math
import matplotlib.pyplot as plt

import numpy as np
from pynwb import NWBFile
from pynwb import NWBHDF5IO

# from std import nwb_metric
import os
import warnings
from matplotlib import interactive
import scipy
from scipy import stats, signal
import pandas as pd
import plotly.graph_objects as go
from scipy.signal import find_peaks


class timeSeriesData:
    def __init__(self, raw_data):
        """
        raw_data refers to the objdata stored in an nwb file
        this class is initialized

        """
        # clean the data of nans
        data = raw_data.data
        numpify = data[:]
        numpify = numpify[~np.isnan(numpify)]
        self.data = numpify
        # update references
        self.length = self.data.size
        self.name = str(raw_data.name)
        self.x_unit = raw_data.timestamps_unit
        self.y_unit = raw_data.unit

    # Summarizing stats

    def mean(self, data):
        return statistics.mean(data)

    def mean_in_bounds(self, low, high):
        return low < statistics.mean(self.data) < high

    def spread(self, data):
        return statistics.stdev(data)

    def spread_in_bounds(self, low, high):
        """
        This function takes in a min and max value for spread so that the scientist can
        check whether the spread of their data falls within a given range

        :param low: the minimum value for standard deviation we should consider
        :param high: the maximum value for standard deviation we should consider
        :return: a boolean, True if actual spread is within bounds and False if it is not within the range
        specified between min and max

        """
        return low < statistics.stdev(self.data) < high

    def proportion_in_bounds(self, low, high):
        """
        returns the percentage of data points that are in the specified range
        """
        counter = 0
        for i in range(self.length):
            if low < self.data[i] < high:
                counter += 1

        return counter / self.length

    def whole_distribution(self, data):
        plt.plot(data)
        plt.xlabel(self.x_unit)
        plt.ylabel(self.y_unit)
        plt.title(self.name)
        plt.show()

    def histogram(self):
        plt.hist(self.data, bins=10)
        plt.gca().set(title=self.name + " Frequency", ylabel="Frequency")
        plt.show()

    def z_scores(self, data):
        """
        create a dataset of equal length but only plot the zscore of the point
        :return: the set of zscores
        """

        z_data = scipy.stats.zscore(data)

        return z_data

    def scatter_plot(self, data):

        x = np.arange(len(data))
        plt.scatter(x, data)
        plt.xlabel(self.x_unit)
        plt.ylabel(self.y_unit)
        plt.title(self.name)
        plt.show()

        return None

    ## need a way to bunch together bins of the seconds and graph them as chunks

    ############# SMOOTHING Algorithms

    def cluster_by_n(self, n):
        """
        this function will group the x axis into bins of n units and plot the average score amongst those n values
        """

        start = 0
        end = n
        bins = np.array([])

        for i in range(0, self.length, n):
            start = i
            end = start + n
            if end >= self.length:
                bins = np.append(bins, [statistics.mean(self.data[start:])])

            else:
                bins = np.append(bins, [statistics.mean(self.data[start:end])])

        print(bins[:10])

        return bins

    def count_jumps(self, data):

        min_data = min(data)
        max_data = max(data)
        jumps = 0
        for i in range(len(data)):
            if i < len(data) - 1:
                diff = abs(data[i] - data[i + 1])
                # either need to increase 0.01 or classify max_data as an outlier
                # in order to reduce number of jumps identified
                if diff > float(0.01 * (max_data - min_data)):
                    jumps += 1
        return jumps

    def savgol_smooth(self, order, data_provided=None):
        # assign the data to the objects main data
        data = self.data
        # assign it to a speicifed dataset if it was provided
        if data_provided:
            data = data

        x = np.arange(len(data))
        y = data

        fig = go.Figure()
        fig.add_trace(
            go.Scatter(
                x=x,
                y=y,
                mode="markers",
                marker=dict(size=6, color="royalblue", symbol="circle-open"),
                name="originaldata",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=x,
                y=signal.savgol_filter(y, 101, order),
                mode="markers",
                marker=dict(size=6, color="mediumpurple", symbol="triangle-up"),
                name="polyfit",
            )
        )

        fig.show()

        return signal.savgol_filter(y, 101, order)

    def n_moving_average(self, n_provided=20, data_provided=None):

        n = 3
        if n_provided:
            n = n_provided

        data = self.data
        # assign it to a speicifed dataset if it was provided
        if data_provided:
            data = data_provided

        ret = np.cumsum(data, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1 :] / n

    ######### Artifact identificaiton Algorithms

    """
    find_peaks(x, height=None, threshold=None, distance=None, prominence=None, width=None, wlen=None, rel_height=0.5, plateau_size=None
    
    
    
    Returns
    peaks : ndarray of Indices of peaks in x that satisfy all given conditions.

    propertiesdict A dictionary containing properties of the returned peaks which were calculated as intermediate results during evaluation of the specified conditions:
        ‘peak_heights’
        If height is given, the height of each peak in x.

        ‘left_thresholds’, ‘right_thresholds’
        If threshold is given, these keys contain a peaks vertical distance to its neighbouring samples.

        ‘prominences’, ‘right_bases’, ‘left_bases’
        If prominence is given, these keys are accessible. See peak_prominences for a description of their content.

        ‘width_heights’, ‘left_ips’, ‘right_ips’
        If width is given, these keys are accessible. See peak_widths for a description of their content.

        ‘plateau_sizes’, left_edges’, ‘right_edges’
        If plateau_size is given, these keys are accessible and contain the indices of a peak’s edges (edges are still part of the plateau) and the calculated plateau sizes.

        New in version 1.2.0.

        To calculate and return properties without excluding peaks, provide the open interval (None, None) as a value to the appropriate argument (excluding distance).
    """

    # weigh a peak against

    # theory of error identification: a set of points for which the surrounding area on either side represents a large

    def plot_peaks(self, peaks, data_p=None):

        data = self.data
        if data_p is not None:
            data = data_p

        minimum = np.amin(data)
        maximum = np.amax(data)

        for peak in peaks:
            plt.vlines(peak, minimum, maximum, linestyles="dashed")

        x = np.arange(len(data))
        plt.scatter(x, data)
        plt.show()

        return

    # def composite_two(self, f1, f2, args_one=None, args_two=None):

    #     if args_one and args_two:

    #     if args_one and not args_two:

    #     if not args_one and args_two:

    #     if not args_one and not args_two:
    #         return f1(f2(self.data))

    # Methods to Access Variables
    def data(self):
        return self.data

    def x_unit(self):
        return self.x_unit

    def y_unit(self):
        return self.y_unit

    def name(self):
        return self.name
