from pynwb import NWBFile
from pynwb import NWBHDF5IO
from std import nwb_metric
import os
import numpy as np
import matplotlib.pyplot as plt
import warnings

# ignore annoying warnings
warnings.filterwarnings("ignore")

# create set of commands for which the shell will actually do something
valid_commands = set(["file", "exit", "help"])

# check if the supplied filepath is a valid path for an nwb document
def valid_path(entry):

    if len(entry) == 1:
        print("no path specified")
        return False

    path = entry[1]

    if len(path) < 4:
        print("not a valid file path")
        return False

    if path[-4:] == ".nwb":
        return True

    print("not a valid file path")
    return False


def analyze(data_container, specified=None):

    for i in range(len(data_container)):
        objdata = data_container[i]
        if objdata.neurodata_type == "TimeSeries":
            print("\n")

            checker = nwb_metric(objdata)
            data = checker.get_filtered_data()
            name = checker.get_data_name()

            if specified and (str(name) not in desired_sets):
                continue

            print("data name: ", name)
            print("sample 10 data points: \n    ", data[:10])
            print("the data type of each data point is: ", type(data[0]))
            print("data consists of ", len(objdata.data), " data points")
            print("Standard Deviation is: \n    ", checker.check_spread())
            print("Saturation is: \n    ", checker.check_saturation())
            print("Prevalence of jumps: \n    ", checker.prob_jumps())
            print("\n")

            plt.plot(data)
            plt.xlabel(str(objdata.timestamps_unit))
            plt.ylabel(str(objdata.unit))
            plt.title(str(name))
            plt.show()


while 1:
    # process input
    x = input(">>> ")
    entry = x.split(" ")
    command = entry[0]

    if command == "":
        continue

    if command == "exit" or command == "e":
        break

    if command not in valid_commands:
        print("not a valid command, for a list of valid commands, do 'help'")
        continue

    if command == "help":
        for element in valid_commands:
            print(element)

    # file processing
    if command == "file":
        if not valid_path(entry):
            continue

        filepath = entry[1]

        # read in valid nwb file
        try:
            io = NWBHDF5IO(filepath, "r")
        except:
            print("not a valid file path")
            continue
        nwbfile = io.read()

        datasets = set()

        data_container = vars(nwbfile)["_AbstractContainer__children"]
        for i in range(len(data_container)):
            objdata = data_container[i]
            if objdata.neurodata_type == "TimeSeries":
                datasets.add(objdata.name)

        if len(entry) == 2 or (len(entry) == 3 and entry[2] == "all"):
            analyze(data_container)

        elif len(entry) == 3 and entry[2] == "options":
            for i in range(len(data_container)):
                objdata = data_container[i]
                if objdata.neurodata_type == "TimeSeries":
                    print(objdata.name)

        elif len(entry) >= 3:
            desired_sets = set(entry[2:])
            flagged = False
            for desired in desired_sets:
                if desired not in datasets:
                    print("that dataset is not a TimeSeries in this nwb file")
                    flagged = True
                    break
            if flagged:
                continue
            analyze(data_container, desired_sets)

        # assign the child field to data container which contains each data set group in a list

        # loops through each dataset printing
        # for i in range(len(data_container)):

        #     objdata = data_container[i]
        #     if objdata.neurodata_type == "TimeSeries":
        #         checker = nwb_metric(objdata.data)
        #         print("data name: ", objdata.name)
        #         print("sample 10 data points: \n    ", objdata.data[:10])
        #         print("the data type of this data is: ", type(objdata.data[0]))

        #         print("\n\n")

        # initialize data reader

        # print(nwbfile)

    # for field in acquisition_fields:\

    # # this code scans the acquisition for timeseries datasets
    # acquisition_fields = set()
    # for obj in nwbfile.acquisition:
    #     acquisition_fields.add(obj)

    # for obj in nwbfile.objects.values():
    #     # print('%s: %s "%s"' % (obj.object_id, obj.neurodata_type, obj.name))
    #     if obj.neurodata_type == "TimeSeries" and obj.name in acquisition_fields:
    #         print(obj.name)

    # if x in acquisition_fields:
    #     data = nwbfile.acquisition[x]
    #     print(data.data[:])
    #     print(
    #         str(x)
    #         + " STD is: "
    #         + str(nwb_metric.check_spread(nwb_metric, data.data[:]))
    #     )
    #     print(
    #         str(x)
    #         + " prob jumps is: "
    #         + str(nwb_metric.prob_jumps(nwb_metric, data.data[:]))
    #     )
    #     print(
    #         str(x)
    #         + " saturation is: "
    #         + str(nwb_metric.check_saturation(nwb_metric, data.data[:]))
    #     )
