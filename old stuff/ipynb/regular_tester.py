import statistics
import math
import matplotlib.pyplot as plt

import numpy as np
from pynwb import NWBFile
from pynwb import NWBHDF5IO

# from std import nwb_metric
import os
import warnings
from matplotlib import interactive
import scipy
from scipy import stats, signal
import pandas as pd
import plotly.graph_objects as go


np.random.seed(1)
warnings.filterwarnings("ignore")


class TimeSeriesData:
    def __init__(self, raw_data):
        """
        raw_data refers to the objdata stored in an nwb file
        this class is initialized

        """
        # clean the data of nans
        data = raw_data.data
        numpify = data[:]
        numpify = numpify[~np.isnan(numpify)]
        self.data = numpify
        # update references
        self.length = self.data.size
        self.name = str(raw_data.name)
        self.x_unit = raw_data.timestamps_unit
        self.y_unit = raw_data.unit

    # Summarizing stats

    def mean(self):
        return statistics.mean(self.data)

    def mean_in_bounds(self, low, high):
        return low < statistics.mean(self.data) < high

    def spread(self):
        return statistics.stdev(self.data)

    def spread_in_bounds(self, low, high):
        """
        This function takes in a min and max value for spread so that the scientist can
        check whether the spread of their data falls within a given range

        :param low: the minimum value for standard deviation we should consider
        :param high: the maximum value for standard deviation we should consider
        :return: a boolean, True if actual spread is within bounds and False if it is not within the range
        specified between min and max

        """
        return low < statistics.stdev(self.data) < high

    # need a function to identify the amount of variability from the mean of each point

    def prob(self, condition, k):
        """
        This function takes in some conditional statement and will comapre it to the parameter k
        which is the portion of data points we expect to be observing this condition

        :param condition: the condition that we will calculate the probability of having across the data points
        :param k: the portion of data points that we expect we will observe this condition across the whole datset
        :return: double: the actual portion of datapoints that observes the condition
        """

        pass

    def proportion_in_bounds(self, low, high):
        """
        returns the percentage of data points that are in the specified range
        """
        counter = 0
        for i in range(self.length):
            if low < self.data[i] < high:
                counter += 1

        return counter / self.length

    def whole_distribution(self):
        plt.plot(self.data)
        plt.xlabel(self.x_unit)
        plt.ylabel(self.y_unit)
        plt.title(self.name)
        plt.show()

    def histogram(self):
        plt.hist(self.data, bins=10)
        plt.gca().set(title=self.name + " Frequency", ylabel="Frequency")
        plt.show()

    def z_scores(self, data):
        """
        create a dataset of equal length but only plot the zscore of the point
        :return: the set of zscores
        """

        x = np.arange(len(data))

        z_data = scipy.stats.zscore(data)

        plt.scatter(x, z_data)
        plt.xlabel(self.x_unit)
        plt.ylabel(self.y_unit)
        plt.title(self.name)
        plt.show()

        return z_data

    ## need a way to bunch together bins of the seconds and graph them as chunks

    def cluster_by_n(self, n):
        """
        this function will group the x axis into bins of n units and plot the average score amongst those n values
        """

        start = 0
        end = n
        bins = np.array([])

        for i in range(0, self.length, n):
            start = i
            end = start + n
            if end >= self.length:
                bins = np.append(bins, [statistics.mean(self.data[start:])])

            else:
                bins = np.append(bins, [statistics.mean(self.data[start:end])])

        print(bins[:10])

        return bins

    def count_jumps(self, data=None):

        if data is None:
            data = self.data

        min_data = min(data)
        max_data = max(data)
        jumps = 0
        for i in range(len(data)):
            if i < len(data) - 1:
                diff = abs(data[i] - data[i + 1])
                # either need to increase 0.01 or classify max_data as an outlier
                # in order to reduce number of jumps identified
                if diff > float(0.01 * (max_data - min_data)):
                    jumps += 1
        return jumps

    def savgol_smooth(self, order, data=None):
        if data:
            data = data
        data = self.data

        x = np.arange(len(data))
        y = data

        fig = go.Figure()
        fig.add_trace(
            go.Scatter(
                x=x,
                y=y,
                mode="markers",
                marker=dict(size=6, color="royalblue", symbol="circle-open"),
                name="originaldata",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=x,
                y=signal.savgol_filter(y, 53, order),
                mode="markers",
                marker=dict(size=6, color="mediumpurple", symbol="triangle-up"),
                name="polyfit",
            )
        )

        fig.show()

        return signal.savgol_filter(y, 53, order)

    # Methods to Access Variables
    def data(self):
        return self.data

    def x_unit(self):
        return self.x_unit

    def y_unit(self):
        return self.y_unit

    def name(self):
        return self.name


nwbfile = False

# specify filepath
filepath = "../nwb_files/test.nwb"

try:
    io = NWBHDF5IO(filepath, "r")
    nwbfile = io.read()
except:
    print("not a valid file path")


data_container = vars(nwbfile)["_AbstractContainer__children"]

raw_data = data_container[2]

obj = TimeSeriesData(raw_data)

obj.z_scores(obj.savgol_smooth(3))
