def add(a, b):
    return a + b


def comp_add(c, func):
    return c + func


print(comp_add(5, add(1, 2)))
