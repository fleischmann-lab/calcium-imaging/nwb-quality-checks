import numpy as np


data = np.random.randn(100)
data_max = np.max(data)


# bools = np.ones((10), dtype=bool)

# print(data)


# saturated: prob(isclose(abs(data), DATA_MAX, 0.02)) > 0.01


#

# usage: Passing the data set along with the value do you want to compare the data set to,
# if you want to specify a proportion then specify the third perimeter, if you want to
# specify an absolute distance past the third parametePassing the data set along with
# the value you want to compare the data set to, if you want to specify a proportion
# then specify the third perimeter, if you want to specify an absolute distance pass
# the third parameter in as none and pass a fourth parameter specifying the absolute distance.


def is_close(data, value, dprop, dabs=None):

    closeness = np.zeros((len(data)), dtype=bool)
    minimum, maximum = np.min(data), np.max(data)
    data_range = maximum - minimum
    if dprop and dabs:
        return "error, cannot specify both dprop and dabs"
    if dprop:
        for i in range(len(data)):
            if value - dprop * data_range < data[i] < value + dprop * data_range:
                closeness[i] = True
    if dabs:
        for i in range(len(data)):
            if value - dabs < data[i] < value + dabs:
                closeness[i] = True

    return closeness


# specify an upper bound if you want the function to tell you whether or not the proportion of positive responses exceeds the upper bound


def prob(events, upper_bound=None):

    length = len(events)
    true_counter = 0
    for i in range(length):
        if events[i] == True:
            true_counter += 1

    if upper_bound:
        if true_counter / length > upper_bound:
            return True
        else:
            return False

    return true_counter / length


# print(prob(is_close(data, data_max, None, 1), 0.05))


# given input string
# saturated: prob(isclose(abs(data), DATA_MAX, 0.02)) > 0.01
# call prob(isclose(abs(data), DATA_MAX, 0.02)), 0.01) and assign the result to variable saturated

# need a way to add to the dispatcher dynamically
dispatcher = {"prob": prob, "is_close": is_close, "data": data}


def gather_contents(string):
    first_bool = True
    first_open = last_close = 0

    for i in range(len(string)):
        if string[i] is "(" and first_bool:
            first_open = i
        if string[i] is ")":
            last_close = i

    return string[first_open + 1 : last_close]


def parse(input):

    outputvar = input.split(":")[0]

    command = input.split(":")[1]

    upper_bound = None

    if ">" in command:
        # prob double parameter case
        upper_bound = float(command.split(">")[1])

    # upper_bound now has the appropriate info info

    first = command.split("(")[0].strip()
    contents = gather_contents(command)

    if first is "prob":
        if contents.split("(")[0].strip() is "is_close":

            is_close_args = gather_contents(contents).split(",")
            data = dispatcher[is_close_args[0].strip()]
            value = float(is_close_args[0].strip())
            dprop = float(is_close_args[0].strip())
            dabs = None
            if len(is_close_args) == 4:
                dabs = float(is_close_args[0].strip())

            exec(
                "%s = %d" % (outputvar, prob(is_close(data, value, drop), upper_bound))
            )

            dispatcher[outputvar] = prob(is_close(data, value, drop), upper_bound)

        else:
            data = dispatcher[is_close_args[0].strip()]
            prob(data, upper_bound)


def get_var(var):
    print(dispatcher[var])
    return dispatcher[var]


parse("saturated: prob(is_close(data, 2, 0.9))")
get_var("saturated")


x = "lol "
exec("%s = %d" % (x, 2))

# print(lol)
