import statistics
import math
import matplotlib.pyplot as plt
import numpy as np


"""
next steps: autochecking for the correct datatypes
need ways to define specific phenomenon (like 90% stimulated outlier)
    would need to know about it in the first place in order to recognize 
"""


class nwb_metric:
    def __init__(self, data):

        # update references
        self.name = data.name
        data = data.data

        # clean the data
        numpify = np.array(data)
        numpify = numpify[~np.isnan(numpify)]

        self.data = numpify

    def get_filtered_data(self):
        return self.data

    def get_data_name(self):
        return self.name

    # checks if more than 10% of data is "close" to min or max values defined as within 1% of the range
    def check_saturation(self):
        max_data = max(self.data)
        min_data = min(self.data)
        close_distance = 0.01 * (max_data - min_data)
        count = 0
        for element in self.data:
            # if abs(element-max_data) <=1:
            #     count += 1

            if (max_data - element < close_distance) or (
                element - min_data < close_distance
            ):
                count += 1

        if count / len(self.data) > 0.01:
            return "The data is highly saturated: " + str(count)
        else:
            return "The data is not highly saturated"

    def check_spread(self):
        return statistics.stdev(self.data)

    def count_jumps(self):
        min_data = min(self.data)
        max_data = max(self.data)
        jumps = 0
        for i in range(len(self.data)):
            if i < len(self.data) - 1:
                diff = abs(self.data[i] - self.data[i + 1])
                # either need to increase 0.01 or classify max_data as an outlier in order to reduce number of jumps identified
                if diff > float(0.01 * (max_data - min_data)):
                    jumps += 1
        return jumps

    def prob_jumps(self):
        num_jumps = self.count_jumps()
        if num_jumps / len(self.data) > 0.001:
            return "There are too many jumps: " + str(num_jumps) + " jumps"
        elif math.ceil(num_jumps / len(self.data)):
            return "There are some jumps: " + str(num_jumps) + " jumps"
        else:
            return "There are no jumps"
