# Andrew and Ishan's nwb quality checks

### to run this repo on your machine

1.  Download the repo
2.  create a virutal environment

        $ python3 -m venv venv
        $ source venv/bin/activate
        $ pip install -r requirements.txt

3.  drop the "quality_checks.py" file into the directory of the project you're working on
4.  at the top of your project file write

        import quality_checks

5.  Create a QualityChecks object with

        qc = QualityChecks("<filepath>", "<["data set names"]>, functions)

6. You're good to go... [checkout this demo for a visual walkthrough](https://drive.google.com/file/d/1BPco7dAQ1X_NcTIXH8KSmGUK7jkOw3G0/view?usp=sharing)