from quality_checks import *


def average(data):
    return statistics.mean(data)


def subtract9(data):
    return data - 9


def add1(data):
    return data + 1


def z_score(data):
    return stats.zscore(data)


def count_larger_than_2(data):
    counter = 0
    for i in range(len(data)):
        if data[i] > 2 or data[i] < -2:
            counter += 1
    return counter / len(data)


qc = QualityChecks("../new_mouse2/Mouse#164.nwb", "all", average)

# flow_object = qc.get_obj("flow")

# print(flow_object.histogram())


# qc.apply_functions(z_score, count_larger_than_2, add1)


# print(qc.file.get_scratch(qc.current_file))

print(qc.get_newest_results())
