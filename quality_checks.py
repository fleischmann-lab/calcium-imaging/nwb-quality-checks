import statistics
import numpy as np
from pynwb import NWBFile
from pynwb import NWBHDF5IO
import scipy
from scipy import stats, signal

# from std import nwb_metric
import os
from os import path
import warnings
from datetime import datetime

np.random.seed(1)
warnings.filterwarnings("ignore")


from matplotlib import interactive


import pandas as pd
import plotly.graph_objects as go
from scipy.signal import find_peaks


class timeSeriesData:
    def __init__(self, raw_data):
        """
        raw_data refers to the objdata stored in an nwb file
        this class is initialized

        """
        # clean the data of nans
        data = raw_data.data
        numpify = data[:]
        numpify = numpify[~np.isnan(numpify)]
        self.data = numpify
        # update references
        self.length = self.data.size
        self.name = str(raw_data.name)
        self.x_unit = raw_data.timestamps_unit
        self.y_unit = raw_data.unit

    # Summarizing stats

    def mean(self, data):
        return statistics.mean(data)

    def mean_in_bounds(self, low, high):
        return low < statistics.mean(self.data) < high

    def spread(self, data):
        return statistics.stdev(data)

    def spread_in_bounds(self, low, high):
        """
        This function takes in a min and max value for spread so that the scientist can
        check whether the spread of their data falls within a given range

        :param low: the minimum value for standard deviation we should consider
        :param high: the maximum value for standard deviation we should consider
        :return: a boolean, True if actual spread is within bounds and False if it is not within the range
        specified between min and max

        """
        return low < statistics.stdev(self.data) < high

    def proportion_in_bounds(self, low, high):
        """
        returns the percentage of data points that are in the specified range
        """
        counter = 0
        for i in range(self.length):
            if low < self.data[i] < high:
                counter += 1

        return counter / self.length

    def whole_distribution(self, data):
        plt.plot(data)
        plt.xlabel(self.x_unit)
        plt.ylabel(self.y_unit)
        plt.title(self.name)
        plt.show()

    def histogram(self):
        plt.hist(self.data, bins=10)
        plt.gca().set(title=self.name + " Frequency", ylabel="Frequency")
        plt.show()

    def z_scores(self, data):
        """
        create a dataset of equal length but only plot the zscore of the point
        :return: the set of zscores
        """

        z_data = scipy.stats.zscore(data)

        return z_data

    def scatter_plot(self, data):

        x = np.arange(len(data))
        plt.scatter(x, data)
        plt.xlabel(self.x_unit)
        plt.ylabel(self.y_unit)
        plt.title(self.name)
        plt.show()

        return None

    ## need a way to bunch together bins of the seconds and graph them as chunks

    ############# SMOOTHING Algorithms

    def cluster_by_n(self, n):
        """
        this function will group the x axis into bins of n units and plot the average score amongst those n values
        """

        start = 0
        end = n
        bins = np.array([])

        for i in range(0, self.length, n):
            start = i
            end = start + n
            if end >= self.length:
                bins = np.append(bins, [statistics.mean(self.data[start:])])

            else:
                bins = np.append(bins, [statistics.mean(self.data[start:end])])

        print(bins[:10])

        return bins

    def count_jumps(self, data):

        min_data = min(data)
        max_data = max(data)
        jumps = 0
        for i in range(len(data)):
            if i < len(data) - 1:
                diff = abs(data[i] - data[i + 1])
                # either need to increase 0.01 or classify max_data as an outlier
                # in order to reduce number of jumps identified
                if diff > float(0.01 * (max_data - min_data)):
                    jumps += 1
        return jumps

    def savgol_smooth(self, order, data_provided=None):
        # assign the data to the objects main data
        data = self.data
        # assign it to a speicifed dataset if it was provided
        if data_provided:
            data = data

        x = np.arange(len(data))
        y = data

        fig = go.Figure()
        fig.add_trace(
            go.Scatter(
                x=x,
                y=y,
                mode="markers",
                marker=dict(size=6, color="royalblue", symbol="circle-open"),
                name="originaldata",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=x,
                y=signal.savgol_filter(y, 101, order),
                mode="markers",
                marker=dict(size=6, color="mediumpurple", symbol="triangle-up"),
                name="polyfit",
            )
        )

        fig.show()

        return signal.savgol_filter(y, 101, order)

    def n_moving_average(self, n_provided=20, data_provided=None):

        n = 3
        if n_provided:
            n = n_provided

        data = self.data
        # assign it to a speicifed dataset if it was provided
        if data_provided:
            data = data_provided

        ret = np.cumsum(data, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1 :] / n

    ######### Artifact identificaiton Algorithms

    """
    find_peaks(x, height=None, threshold=None, distance=None, prominence=None, width=None, wlen=None, rel_height=0.5, plateau_size=None
    
    
    
    Returns
    peaks : ndarray of Indices of peaks in x that satisfy all given conditions.

    propertiesdict A dictionary containing properties of the returned peaks which were calculated as intermediate results during evaluation of the specified conditions:
        ‘peak_heights’
        If height is given, the height of each peak in x.

        ‘left_thresholds’, ‘right_thresholds’
        If threshold is given, these keys contain a peaks vertical distance to its neighbouring samples.

        ‘prominences’, ‘right_bases’, ‘left_bases’
        If prominence is given, these keys are accessible. See peak_prominences for a description of their content.

        ‘width_heights’, ‘left_ips’, ‘right_ips’
        If width is given, these keys are accessible. See peak_widths for a description of their content.

        ‘plateau_sizes’, left_edges’, ‘right_edges’
        If plateau_size is given, these keys are accessible and contain the indices of a peak’s edges (edges are still part of the plateau) and the calculated plateau sizes.

        New in version 1.2.0.

        To calculate and return properties without excluding peaks, provide the open interval (None, None) as a value to the appropriate argument (excluding distance).
    """

    # weigh a peak against

    # theory of error identification: a set of points for which the surrounding area on either side represents a large

    def plot_peaks(self, peaks, data_p=None):

        data = self.data
        if data_p is not None:
            data = data_p

        minimum = np.amin(data)
        maximum = np.amax(data)

        for peak in peaks:
            plt.vlines(peak, minimum, maximum, linestyles="dashed")

        x = np.arange(len(data))
        plt.scatter(x, data)
        plt.show()

        return

    # def composite_two(self, f1, f2, args_one=None, args_two=None):

    #     if args_one and args_two:

    #     if args_one and not args_two:

    #     if not args_one and args_two:

    #     if not args_one and not args_two:
    #         return f1(f2(self.data))

    # Methods to Access Variables
    def data(self):
        return self.data

    def x_unit(self):
        return self.x_unit

    def y_unit(self):
        return self.y_unit

    def name(self):
        return self.name


class QualityChecks:
    # things to error check:
    """
    desired sets are actually in the file
    filepath is an nwb file
    each function can pass without error
    make sure there actually is a time series in the nwb file
    """

    # first function applied first, second function applied second etc.

    def __init__(self, filepath, desired_sets="all", *args):
        """
        input:
        str: filepath: a string specifying the location of the desired nwb file
        list(str): desired_sets: A list of strings corresponding to the names of the specific timeseries datasets in the nwb file that the user wants to check. If the user would like to run all of them, set parameter to string "all"
        function:function_one a user supplied function to be applied to all timeseries data in the set
        """

        self.accepting_new_functions = True

        self.filepath = filepath
        self.desired_sets = desired_sets
        self.args = args

        # nwbfile = None

        try:
            io = NWBHDF5IO(filepath, mode="a")
            nwbfile = io.read()
        except:
            print("not a valid file path")

        if not path.exists(filepath):
            raise ValueError("not a valid file path!")

        self.file = nwbfile
        self.io = io

        self.current_file = self.get_newest_file_name()

        # narrows data container to only contain TimeSeries Data from the given nwb file

        # only examines the timeseries named if the parameter is provided
        data_container = [
            x
            for x in vars(nwbfile)["_AbstractContainer__children"]
            if x.neurodata_type == "TimeSeries"
        ]
        if desired_sets != "all":

            valid_names = []
            for x in vars(nwbfile)["_AbstractContainer__children"]:
                if x.neurodata_type == "TimeSeries":
                    valid_names.append(x.name)

            if not isinstance(desired_sets, list):
                raise ValueError(
                    "please check that your desired sets are in the correct format: ['flow', 'wheel'] for example"
                )

            for name in desired_sets:
                if name not in valid_names:
                    raise ValueError(
                        "please check that your desired sets are in the correct format: ['flow', 'wheel'] for example"
                    )

            data_container = [
                x
                for x in vars(nwbfile)["_AbstractContainer__children"]
                if (x.neurodata_type == "TimeSeries" and x.name in desired_sets)
            ]

        data_sets = {}
        # this loop turns every timeseries in the nwb file into a timeseries data object
        for data in data_container:
            data_sets[data.name] = timeSeriesData(data)

        self.data_sets = data_sets
        if self.accepting_new_functions:
            self.apply_functions(*args)

    def get_data_dict(self):
        return self.data_sets

    # def get_current_file(self):
    #     if self.get_newest_file_name() == '-1':
    #         return "there is no quality_checks file yet"
    #     return self.get_newest_results().data

    # allows the user to access any of the object's built in methods
    def get_obj(self, data_name):
        return self.data_sets[data_name]

    def apply_functions(self, *args):

        io = self.io
        nwbfile = self.file
        data_sets = self.data_sets
        if args and self.accepting_new_functions:
            function_chain_error = False

            # compiles the list of functions in the order they're applied
            functions = ""
            for arg in args:
                functions += arg.__name__ + "-> "

            # records the current time
            time = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

            # creates a dictionary mapping each timeseries to a set of functions, results, and times at which the mapping took place
            results = {}
            for set in data_sets:
                result = data_sets[set].data

                for func in args:
                    try:
                        result = func(result)
                    except:
                        print(
                            "your function sequence threw an error. Check the inputs and outputs of your function chain!"
                        )
                        function_chain_error = True

                if type(result).__module__ == np.__name__:
                    try:
                        result = result[:10]
                    except:
                        "final output not a datafile"

                results[set] = [functions, result, time]

            if not function_chain_error:
                stringify = str(results)
                # if nothing has been added yet

                if self.current_file is None:
                    # print(
                    #     "creating the first quality checks file in this nwb files history"
                    # )

                    self.current_file = "0"
                    filler_data = [
                        "{dataset_name:[series of tests, results, date and time of resuls]}",
                        stringify,
                    ]
                    nwbfile.add_scratch(
                        filler_data,
                        name="0",
                        notes="a log of results from running quality checks on the data",
                    )
                    io.write(nwbfile)
                    io.close()
                else:
                    most_recent = self.get_newest_results().data
                    curr = np.append(
                        np.array(most_recent), np.array([stringify])
                    ).tolist()

                    self.current_file = str(int(self.current_file) + 1)

                    nwbfile.add_scratch(
                        curr,
                        name=self.current_file,
                        notes="a log of results from running quality checks on the data",
                    )
                    # self.current_file = i
                    # print(
                    #     "creating the "
                    #     + self.current_file
                    #     + "th file in the nwb files history"
                    # )
                    io.write(nwbfile)
                    io.close()
                self.accepting_new_functions = False

    def get_newest_results(self):
        """
        This function gets the current scratch file for the nwb object
        """
        return self.file.get_scratch(self.current_file)

    def get_newest_file_name(self):

        nwbfile = self.file

        history = True
        i = "0"
        try:
            nwbfile.get_scratch(i)
        except:
            # print("there's no previous runs of quality_checks here")
            history = False
            return None

        while self.file_exists(nwbfile, i) and history:
            i = str(int(i) + 1)

        return str(int(i) - 1)

    def file_exists(self, nwbfile, name):
        exists = True
        try:
            nwbfile.get_scratch(name)
        except:
            exists = False
        return exists


# qc = QualityChecks("../mouse/Mouse#164.nwb", ["flow"])
# qc.apply_functions(z_score, count_larger_than_2)


# print(qc.file.get_scratch(qc.current_file))


# io = NWBHDF5IO("new_mouse/Mouse#163.nwb", mode = 'r')
# nwbfile = io.read()
# print(np.array(nwbfile.get_scratch('0').data))
